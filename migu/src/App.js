import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <div className="container">
        <header className = "header">
          <div className="header-top">
            <div className="logo"></div>
            <div className="nav-main">
              <ul className="nav-container">
                <li className="nav-item on">
                  <Link to ="/">音乐</Link>
                  <span className="line"></span>
                </li>
                <li className="nav-item">
                  <Link to ="/video">现场</Link>
                  <span className="line"></span>
                </li>
                <li className="nav-item">
                  {/* <Link to="/user">我的</Link> */}
                  <Link to="/user">我的</Link>
                  <span className="line"></span>
                </li>
              </ul>
            </div>
            <div className="search">
              <input type="text" placeholder="搜索歌曲、歌手、MV" className="search_ipt"/>
              <span className="btn_search iconfont icon-sousuo"></span>
            </div>
            <div className="nav-operation">
              <ul className="nav-container">
                <li className="nav-item">
                  <Link className="nav-item-link" to="/v3/music/service/platinum" target="_blank"><i className="iconfont icon-zuanshihuiyuan"></i><span>白金会员</span></Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-item-link" to="/v2/app" target="_blank"><i className="iconfont icon-kehuduan"></i><span>客户端</span></Link>
                </li>
                <li className="nav-item nav-diy">
                  <Link className="nav-item-link" to="/v3/music/service/vrbt" target="_blank"><i className="iconfont icon-cailing"></i><span>视频彩铃</span></Link>
                </li>
                <li className="nav-item" id="login-user-info">
                  <Link className="nav-item-link J_login" to="/login"><i className="iconfont icon-denglu-copy"></i><span>登录</span></Link>
                </li>
              </ul>
            </div>
          </div>
          <div className="nav">
            <div className="nav-top">
              <ul className="kind">
                <li className="kind-index"><Link to="/v3">首页</Link></li>
                <li className="kind-index"><Link to="/v3">数字专辑</Link></li>
                <li className="kind-index"><Link to="/v3">歌单</Link></li>
                <li className="kind-index"><Link to="/v3">专辑</Link></li>
                <li className="kind-index"><Link to="/v3">榜单</Link></li>
                <li className="kind-index"><Link to="/v3">歌手</Link></li>
                <li className="kind-index"><Link to="/v3">彩铃</Link></li>
                <li className="kind-index"><Link to="/v3">咪咕出品</Link></li>
                <li className="kind-index"><Link to="/v3">音乐人</Link></li>
              </ul>
            </div>
          </div>
        </header>
        <div className="banner"></div>
        <div className="section"></div>
        <div className="footer">
          <div className="footer-content">
            <div className="footer-content-left">
              <p className="migu_family">
                咪咕音乐家族
              </p>
              <ul className="migu_family_kind">
                <li>手机版</li>
                <li>IVR</li>
                <li>企业彩铃</li>
                <li>咪咕爱唱（TV版）</li>
                <li>个性彩铃</li>
              </ul>
            </div>
            <div className="migu_family_middle">
              <p className="connect_we">联系我们</p>
              <ul className="content">
                <li>业务合作</li>
                <li>开放平台</li>
                <li>常见问题</li>
                <li>咪咕音乐合作人</li>
                <li>联系我们</li>
              </ul>
            </div>
            <div className="migu_family_right">
              <h3>咪咕客户端</h3>
              <ul className="migu_Client">
                <li>
                  <p>咪咕音乐</p>
                  <img src={require('./img/migumusic_ewm.png')} alt="" />
                </li>
                <li>
                  <p>咪咕视频</p>
                  <img src={require('./img/miguvideo_ewm.png')} alt="" />
                </li>
              </ul>
            </div>
          </div>
          <div className="footer-bottom">
              <ul className="footer-nav">
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">咪咕文化</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">咪咕音乐简介</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">版权声明</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">新闻动态</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">友情链接</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">网站地图</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">服务协议</Link></li>
                <li><Link to="http://www.migu.cn/index.html;jsessionid=6903C037016A7019942F9307AB5F2CDA">隐私权政策</Link></li>
              </ul>
              <ul className="c-auth">
                <li>蜀ICP备15012512号-1</li>
                <li>川网文[2012]0762-026</li>
                <li>网络视听许可证0112648号</li>
                <li>增值业务许可证A2.B1.B2-20100001</li>
              </ul>
              <div className="c-info">Copyright © 2005 - 2018 咪咕音乐有限公司</div>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
