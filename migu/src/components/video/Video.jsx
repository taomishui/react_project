import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import 'element-theme-default';
import 'antd/dist/antd.css';
import Exclusive from './child/Exclusive';
import Default from './child/Default';
import VocalConcert from './child/VocalConcert';
import MV from './child/MV';
import MVdetails from './child/MVdetails';
class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
        list: [
            {
              path: '/video/default',
              name: '首页'
            },
            {
              path: '/video/vocal-concert',
              name: '演唱会'
            },
            {
            path: '/video/exclusive',
            name: '独家放送'
            },
            {
            path: '/video/mv',
            name: 'MV'
            },
            {
              path: '/video/mvdetails',
              name: '详情'
            }
          ]
    }
  }

  render () {
    return (
      <div className = "container">
        <Switch>
          <Route path="/video/default" component = { Default } />
          <Route path="/video/exclusive" component = { Exclusive } />
          <Route path="/video/vocal-concert" component = { VocalConcert } />
          <Route path="/video/mv" component = { MV } />
          <Route path="/video/mvdetails/:id" component = { MVdetails } />
          <Redirect from='/video' to='/video/default'/>
        </Switch>
      </div>
    )
  }
}

export default Com;