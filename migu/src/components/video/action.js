export default {
    getMvListData () {
      return function (dispatch, getState) {
        fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=52&offset=0')
        .then(res => res.json())
        .then(data => {
            console.log(data)
            // localStorage.setItem('list',JSON.stringify(data))
            dispatch({
                type: "CHANGE_MV_LIST",
                data: data.data
            })
        })
      }
    },
    getExListData () {
        return function (dispatch, getState) {
          fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=52&offset=52')
          .then(res => res.json())
          .then(data => {
              console.log(data)
              // localStorage.setItem('list',JSON.stringify(data))
              dispatch({
                  type: "CHANGE_EX_LIST",
                  data: data.data
              })
          })
        }
      }
}