const reducer = (state = {
    mvlist: [],
    Exlist: []
  }, action) => {
    // const { mvlist } = state;
    // const { Exlist } = state;
    const { type, data } = action;
    switch (type) {
      case 'CHANGE_MV_LIST':
        return {
            mvlist: data
        };
      case 'CHANGE_EX_LIST':
        return {
            Exlist: data
        };
      default: 
        return state;
    }
  }
  
  export default reducer;