import React, { Component } from 'react';
import { Pagination } from 'element-react';
import Titlelist from '@/components/video/common/Titlelist';
import EXitem from '@/components/video/common/EXitem';
import './video.scss'
class Com extends Component {
  constructor (props) {
    super(props);
    // this.titleFn = this.titleFn.bind(this);
    this.state = {
      title:[{"name":"频道","type":["全部","非凡音乐汇","喋喋不休","乐行者","饭团自制","咪咕星秀场","乐访","乐来越疯狂"]}],
    }
  }
  
  goDetail (index) {
    console.log(this.props.mvlist[index].id)
    const id = this.props.mvlist[index].id;
    this.props.history.push('/video/mvdetails/'+id)
  }
  componentDidMount () {
    this.props.getExListData()
  }
  render () {
    const { Exlist } = this.props;
    return (
     <div className="container">
        <div className="exclusive">
        <Titlelist  list= {this.state.title}/>
            <div className="title">
              全部
            </div>
            <ul className = "videolist">
              {
                Exlist.map((item, index) => {
                  return (
                    <EXitem key={ index } data = { item } { ...this.props }/>
                  )
                })
              }
            </ul>
            <Pagination layout="prev, pager, next" total={80} small={true}/>
         </div>
     </div>
    )
  }
}

export default Com;