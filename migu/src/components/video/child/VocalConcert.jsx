import React, { Component } from 'react';
import { Pagination } from 'element-react';
import { Link } from 'react-router-dom';
import Titlelist from '@/components/video/common/Titlelist';
import './video.scss'
class Com extends Component {
  constructor (props) {
    super(props);
    // this.titleFn = this.titleFn.bind(this);
    this.state = {
      singer:[{"name":"歌手","type":["全部","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]}],
      singerlist:[],
      title1:[{"name":"频道","type":["全部","明星专访","男神集中营","音乐颁奖盛典","SNH48女团","咪咕音乐现场","最燃音乐节"]}],
      title2:[{"name":"时间","type":["全部","2017年","2018年","2019年"]}],
      list:[]
    }
  }


  componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=52&offset=60')
      .then(res => res.json())
      .then(data => {
        // console.log(data.data.songs)
        this.setState({
          list: data.data
        })
      })
      fetch('https://api.bzqll.com/music/netease/search?key=579621905&s=张&type=songer&limit=8&offset=16')
      .then(res => res.json())
      .then(data => {
        // console.log(data.data)
        this.setState({
          singerlist: data.data
        })
      })
    }

  render () {
    return (
     <div className="container">
        <div className="vocalconcert">
         <Titlelist  list= {this.state.singer}/>
          <ul className = "singerlist">
            {
              this.state.singerlist.map((item, index) => {
                return (
                  <li key = { index }>                 
                      <img src={ item.pic } alt="" />
                      <p>{ item.singer }</p>
                  </li>
                )
              })
            }
          </ul>
          <Titlelist  list= {this.state.title1}/>
          <Titlelist  list= {this.state.title2}/>
            <div className="title">
              全部演出
              <ul className="titleTab">
                <li className="active">最新发布</li>
                <li>最多播放</li>
              </ul>
            </div>
            <ul className = "videolist">
              {
                this.state.list.map((item, index) => {
                  return (
                    <li key = { index }>
                      <div className = "thumb">
                        <Link to ="/video/mvdetails">
                          <div className = "video-cover">
                            <em><img src={ item.pic } alt="" /></em>
                            <span className = "play-btn action-btn playlist-play">
                              <img src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                            </span>
                            <div className="img-mask"></div>
                          </div>
                        </Link>
                        <div className="video-name">{ item.name }</div>
                        <div className="video-infos">
                          <span className="video-info-l">
                            { item.singer }
                          </span>
                          <span className="video-info-r">
                            { item.id-10800000 }W
                          </span>
                          <div className="video-time-place">
                            <span>2018-11-30</span>
                            <i>上海</i>
                          </div>
                        </div>
                      </div>
                    </li>
                  )
                })
              }
            </ul>
            <Pagination layout="prev, pager, next" total={80} small={true}/>
         </div>
     </div>
    )
  }
}

export default Com;