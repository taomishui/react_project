import { connect} from 'react-redux';
import UI from './MVUI';
import action from './../action';

const mapStateToProps = (state) =>{
  return {
    mvlist: state.mv.mvlist
  }
}
const mapDispatchToProps = (dispatch) =>{
  return {
    getMvListData () {
      dispatch(action.getMvListData())
    }
  }
}

const Com = connect(
  mapStateToProps,
  mapDispatchToProps
)(UI)

export default Com;