import React, { Component } from 'react';
import { Pagination } from 'element-react';
import Titlelist from '@/components/video/common/Titlelist';
import MVitem from '@/components/video/common/MVitem';
import './video.scss'
class Com extends Component {
  constructor (props) {
    super(props);

    this.state = {
      title1:[{"name":"风格","type":["全部","流行","民谣","摇滚","R&B","电子","纯音乐","轻音乐","乡村","爵士","拉丁","古典"]}],
      title2:[{"name":"地域","type":["全部","内地","欧美","港台","日韩","其他"]}],
      title3:[{"name":"主题","type":["全部","影视","搞笑","舞蹈","唯美","综艺","舞台戏剧","演奏","颁奖礼","现场演出","中国特色","二次元","KTV版"]}],
    }
  }

  componentDidMount () {
    this.props.getMvListData()
  }
  goDetail (index) {
    console.log(this.props.mvlist[index].id)
    const id = this.props.mvlist[index].id;
    this.props.history.push('/video/mvdetails/'+id)
  }
  render () {
      const { mvlist } = this.props;
    //   const { id } = this.props.mvlist.index;
    return (
     <div className="container">
        <div className="mv">
          <Titlelist  list= {this.state.title1}/>
          <Titlelist  list= {this.state.title2}/>
          <Titlelist  list= {this.state.title3}/>
            <div className="title">
              全部MV
              <ul className="titleTab">
                <li className="active">最新发布</li>
                <li>最多播放</li>
              </ul>
            </div>
            <ul className = "videolist">
              {
                mvlist.map((item, index) => {
                  return (
                    <MVitem key={ index } data = { item } { ...this.props }/>
                  )
                })
              }
            </ul>
            <Pagination layout="prev, pager, next" total={80} small={true}/>
         </div>
     </div>
    )
  }
}

export default Com;