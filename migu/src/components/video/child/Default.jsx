import React, { Component } from 'react';
import './default.scss';
import { Carousel } from 'element-react';
import Banner from '@/components/video/common/Banner';
import Banner2 from '@/components/video/common/Banner2';
import Banner3 from '@/components/video/common/Banner3';
import { Link } from 'react-router-dom';

class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
      current:0,
      current1:0,
      current2:0,
      current3:0,
      prediction:["1.01 - 1.20","3.01 - 3.15"],
      exclusive:["非凡音乐汇","乐行者","喋喋不休","饭团自制"],
      pick1:["最佳现场·听独特女声","引爆狂欢·熊猫抢先听"],
      pick2:["伴你入睡 · 今夜好眠","最好的礼物 · 和你在一起","从书本到荧幕 · 小说改编影视原声","神道妖道 · 自求人间道"],
      bannerlist1:[],
      swiperlist:[]
    }
  }
  
  titleFn (index) {
    console.log(this)     
     this.setState({
       current:index
     })
   }

  titleFn1 (index) {
    console.log(this)     
     this.setState({
       current1:index
     })
   }
   titleFn2 (index) {
    console.log(this)     
     this.setState({
       current2:index
     })
   }
   titleFn3 (index) {
    console.log(this, index)     
     this.setState({
       current3:index
     })
   }

   componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=6&offset=34')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        this.setState({
          bannerlist1: data.data
        })
      })
      fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=4&offset=34')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        this.setState({
          swiperlist: data.data
        })
      })
    }

  render () {
    return (
     <div className="video-index-page">
        <div className="vi-banner">
          <div className="vi-banner-con">
            <div className="swiper-container">
              <video className="fp-engine" preload="metadata"  src="https://api.bzqll.com/music/netease/mvUrl?id=10785876&key=579621905" x-webkit-airplay="allow" autoPlay></video>
              <div className="info mask">
                  <span className="title">周深《缘起》MV首播</span>
                  <i className="enter">进入放映间</i>
              </div>
            </div>
            <div className="swiper-pager">
              <ul className="active">               
                {
                  this.state.swiperlist.map((item, index) => {
                    return (
                      <li key={index}>
                        <div className="thumb"><img src={ item.pic } alt=""/></div>
                        <div className="info mask">
                          <span id="J_LiveFlag10094" className="flag J_LiveFlag" data-id="10094">MV</span>
                          <i>{ item.name }</i>
                        </div>
                      </li>
                    )
                  })
                }
              </ul>
            </div>
          </div>
        </div>

        <div className="vi-section vi-live-prediction">
          <div className="vi-container">
            <div className="vi-section-title">演唱会直播</div>
            <div className="prediction-dates">             
              <ul>
                {
                  this.state.prediction.map((item, index) => {
                    return (
                      <li className={this.state.current===index?'active':''} key={index} onClick={this.titleFn.bind(this,index)}>
                        { item }
                      </li>
                    )
                  })
                }
              </ul>
            </div>
          </div>  
          <div className="swiper-container-wrapper">
            <Banner3 />
          </div>        
        </div>

        <div className="vi-section vi-live-playback">
          <div className="vi-container">
            <div className="vi-section-title">
              演唱会回顾
                <Link className="more" to ="/video/vocal-concert">
                  更多
                  <i className="iconfont icon-jiantouyou"></i>
                </Link>
            </div>
          </div> 
          <div className="swiper-container-wrapper">
            <Banner2 />
          </div>  
        </div>

        <div className="vi-section vi-exclusive">
          <div className="vi-container">
            <div className="vi-section-title">
              独家放送
              <Link className="more" to ="/video/exclusive">
                  更多
                  <i className="iconfont icon-jiantouyou"></i>
              </Link>
            </div>
          </div> 
          <ul className="tabs-nav">
            {
              this.state.exclusive.map((item, index) => {
                return (
                  <li className={this.state.current1===index?'active':''} key={index} onClick={this.titleFn1.bind(this,index)}>
                    { item }
                  </li>
                )
              })
            }
          </ul>
          <div className="swiper-container-wrapper">
            <div className="demo-4 medium">
              <Carousel interval="4000" type="card">
                {
                  this.state.bannerlist1.map((item, index) => {
                    return (
                      <Carousel.Item key={index}>
                        <h3><img src={ item.pic } alt="" /></h3>
                      </Carousel.Item>
                    )
                  })
                }
              </Carousel>
            </div>     
          </div>  
        </div>

        <div className="vi-section vi-section vi-live-pick">
          <div className="vi-container">
            <div className="vi-section-title">
              LIVE 精选
              <Link className="more" to ="/video/mv">
                  更多
                  <i className="iconfont icon-jiantouyou"></i>
              </Link>
            </div>
          </div> 
          <ul className="tabs-nav">
            {
              this.state.pick1.map((item, index) => {
                return (
                  <li className={this.state.current2===index?'active':''} key={index} onClick={this.titleFn2.bind(this,index)}>
                    { item }
                  </li>
                )
              })
            }
          </ul>
          <div className="swiper-container-wrapper">
             <Banner />
          </div>  
        </div>

        <div className="vi-section vi-mv-pick">
          <div className="vi-container">
            <div className="vi-section-title">
              MV 精选
              <Link className="more" to ="/video/mv">
                  更多
                  <i className="iconfont icon-jiantouyou"></i>
              </Link>
            </div>
          </div> 
          <ul className="tabs-nav">
            {
              this.state.pick2.map((item, index) => {
                return (
                  <li className={this.state.current3===index?'active':''} key={index} onClick={this.titleFn3.bind(this,index)}>
                    { item }
                  </li>
                )
              })
            }
          </ul>
          <div className="swiper-container-wrapper">
            <Banner />
          </div>  
        </div>

     </div>
    )
  }
}

export default Com;