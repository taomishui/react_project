import { connect} from 'react-redux';
import UI from './EXUI';
import action from './../action';

const mapStateToProps = (state) =>{
  return {
    Exlist: state.mv.Exlist
  }
}
const mapDispatchToProps = (dispatch) =>{
  return {
    getExListData () {
      dispatch(action.getExListData())
    }
  }
}

const Com = connect(
  mapStateToProps,
  mapDispatchToProps
)(UI)

export default Com;