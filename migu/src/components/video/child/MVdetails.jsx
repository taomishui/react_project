import React, { Component } from 'react';
import Comment from '@/components/video/common/Comment';
import { Tabs } from 'element-react';
import './detail.scss';
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {
      detail: "",
      videolist:[],
      mvlist:[]
    }
  }

  componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=4&offset=20')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        this.setState({
          videolist: data.data
        })
      })
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=7&offset=30')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        this.setState({
          mvlist: data.data
        })
      })
      console.log(this.props.match.params.id)
      const pid = this.props.match.params.id
      fetch("https://api.bzqll.com/music/netease/mv?key=579621905&id="+pid)
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        this.setState({
          detail: data.data
        })
      })
    }


  render () {
    const { url, name, singer } = this.state.detail;
    return (
      <div className = "detailBox">
        <div className ="watch">
          <div className = "container">
            <video className="fp-engine" preload="metadata"  src={ url } x-webkit-airplay="allow" controls autoPlay></video>
            <div className="barrage-form-container">
              <div className="barrage-form">
                <div id="J_BarrageInput" className="form-control" data-count-limit="50">
                  <span className="count-limit">0/50</span>
                  <input type="text" placeholder="发送弹幕一起high！"/>
                </div>
                <i className="barrage-settings iconfont icon-shezhi"></i>
                <i className="barrage-send">发送</i>
              </div>
            </div>

            <div className="watch-sidebar">
              <h5 className="relative-mv-title">相关视频</h5>
              <div className="slimScrollDiv">
                <ul className="relative-mv-list">
                  {
                    this.state.mvlist.map((item, index) => {
                    return (
                        <li key={index}>
                          <span className="thumb"><img src={ item.pic } alt=""/></span>
                          <div className="info">
                            <p className="caption">{ item.name }</p>
                            <p className="singers">{ item.singer }</p>
                          </div>
                        </li>
                    )
                    })
                  }
                </ul>
              </div>
            </div>
            <div className="mv-info">
              <div className="title">
                <span className="name">{ name }</span>
                <h6 className="crbt"><em className="iconfont icon-lingsheng"></em>彩铃订购</h6>
                <h6 className="download"><em className="iconfont icon-xiazai"></em>歌曲下载</h6>
                <h6 className="share"><em className="iconfont icon-fenxiang1"></em>分享</h6>
                <h6 className="favor"><em className="iconfont icon-xin"></em>收藏</h6>
              </div>
              <div className="singer">{ singer }</div>
            </div>
          </div>
        </div>

        <div className="hot-playbacks">
          <h2 className="title">热门视频</h2>
          <ul className="videos-list">
              {
                this.state.videolist.map((item, index) => {
                  return (
                    <li key = { index }>
                        <div className = "video-cover">
                          <em><img src={ item.pic } alt="" /></em>
                          <span className = "play-btn action-btn playlist-play">
                            <img src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                          </span>
                          <div className="img-mask"></div>
                        </div>
                        <div className="video-name">{ item.name }</div>
                        <div className="video-infos">
                          <div className="video-info-l">
                            { item.singer }
                          </div>
                          <div className="video-info-r">
                            <i className="iconfont icon-yanjing"></i>171.1W
                          </div>                        
                        </div>
                    </li>
                  )
                })
              }
          </ul>
        </div>
        <div className="downloads">
          <img src="//cdnmusic.migu.cn/cmsupload/12002/upload/graphicLink/20180410113408341528.jpg" alt=""/>
        </div>
        <Comment />
        <div className="comment-list">
          <Tabs activeName="2" onTabClick={ (tab) => console.log(tab.props.name) }>
            <Tabs.Pane label="热门评论" name="1">
              <div className="no-comment">
                <span className="iconfont icon-pinglun-copy-copy"></span>
                <p>路过围观，不如一起促膝长谈</p>
              </div>
            </Tabs.Pane>
            <Tabs.Pane label="最新评论" name="2">
              <div className="no-comment">
                <span className="iconfont icon-pinglun-copy-copy"></span>
                <p>路过围观，不如一起促膝长谈</p>
              </div>
            </Tabs.Pane>
          </Tabs>
        </div>
      </div>
    )
  }
}

export default Com;