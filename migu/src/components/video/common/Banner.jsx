import React, { Component } from 'react';
import './banner.scss'
import { Carousel } from 'element-react';
import 'element-theme-default';

class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
        bannerlist:[]
    }
  }
 
  componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=8&offset=64')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        
        this.setState({
          bannerlist: data.data
        })
      })
    }

  render () {
      console.log(this.state.bannerlist)
    return (
        <div className="demo-2 medium">
            <Carousel indicatorPosition="none">
            {
                [1,2,3].map((item, index) => {
                return (
                    <Carousel.Item key={index}>
                    <div className="banners">
                    <ul>
                        {
                            this.state.bannerlist.map((itx, inx) => {
                            return (
                                <li key={inx}>
                                   <em>
                                       <img className="lazy-image" src={ itx.pic } alt="" />
                                       <span>
                                          <img className="lazy-play" src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                                       </span>
                                   </em>
                                   <h5> { itx.name }</h5>
                                   <div className="live">
                                      <div className="live-singer">{ itx.singer }</div>
                                      <div className="live-playcount"><span className="iconfont icon-bofang2"></span>18.5W</div>
                                   </div>
                                </li>
                            )
                            })
                        }
                        </ul>
                    </div>
                    </Carousel.Item>
                )
                })
            }
            </Carousel>
      </div>
    )
  }
}

export default Com;