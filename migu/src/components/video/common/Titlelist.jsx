import React, {Component} from 'react';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
        current:0
    }
  }

  titleFn (inx) {
    console.log(inx)    
     this.setState({
       current:inx
     })
   }

  render (){
    return(
      <div className="titlelist">
       {
            this.props.list.map((item, index) => {
              return (
              <div className="filter" key={index}>
                <div className="tag-name" >{item.name}</div>
                <ul>
                  {
                    item.type.map((itx, inx) => {
                      return (
                        <li className={ this.state.current===inx?'active':''} key={inx} onClick={this.titleFn.bind(this,inx)}>
                          { itx }
                        </li>
                      )
                    })
                  }
                </ul>
              </div>
              )
           })
        }
        
      </div>
    )
  }
}
export default Com;