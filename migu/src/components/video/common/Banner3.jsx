import React, { Component } from 'react';
import './banner.scss'
import { Carousel } from 'element-react';
import 'element-theme-default';

class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
        bannerlist:[]
    }
  }
 
  componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=3&offset=82')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        
        this.setState({
          bannerlist: data.data
        })
      })
    }

  render () {
      console.log(this.state.bannerlist)
    return (
        <div className="demo-1 medium">
            <Carousel indicatorPosition="none">
            {
                [1,2,3,4].map((item, index) => {
                return (
                    <Carousel.Item key={index}>                   
                    <div className="banners">
                        <ul>
                            {
                                this.state.bannerlist.map((itx, inx) => {
                                return (
                                    <li key={inx}>
                                    <em>
                                        <img className="lazy-image" src={ itx.pic } alt="" />
                                        <span>
                                            <img className="cf-play" src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                                        </span>
                                    </em>
                                    <div className="info"><h6>{ itx.name }</h6> <button>预约</button></div>
                                    <div className="live">                                     
                                        2019-01-11   19:30:00
                                    </div>
                                    </li>
                                )
                                })
                            }
                        </ul>
                    </div>
                    </Carousel.Item>
                )
                })
            }
            </Carousel>
      </div>
    )
  }
}

export default Com;