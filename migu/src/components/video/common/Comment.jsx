import React, { Component } from 'react';
import "./comment.scss";
class Com extends Component {
  constructor (props) {
    super(props)
    this.state = {}
  }
  

  render () {
    return (
        <div className="comment">
          <h2 className="title">热门评论</h2>
          <div className="form-group">
            <span className="count-limit">0/80</span>
            <textarea id="J_CommentBody" name="commentBody" placeholder="快来评论你看视频的感受吧！"></textarea>
          </div>
          <div className="text-right">
            <button className="btn-text" type="submit">发表评论</button>
          </div>
        </div>
    )
  }
}


  export default Com;