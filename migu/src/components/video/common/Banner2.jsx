import React, { Component } from 'react';
import './banner.scss'
import { Carousel } from 'element-react';
import 'element-theme-default';

class Com extends Component {
  constructor (props) {
    super(props);
    this.state = {
        bannerlist:[]
    }
  }
 
  componentDidMount () {
    fetch('https://api.bzqll.com/music/netease/topMvList?key=579621905&limit=4&offset=46')
      .then(res => res.json())
      .then(data => {
        console.log(data.data)
        
        this.setState({
          bannerlist: data.data
        })
      })
    }

  render () {
      console.log(this.state.bannerlist)
    return (
        <div className="demo-3 medium">
            <Carousel indicatorPosition="outside">
            {
                [1,2,3,4,5,6].map((item, index) => {
                return (
                    <Carousel.Item key={index}>
                    <div className="banners">
                        <ul>
                            {
                                this.state.bannerlist.map((itx, inx) => {
                                return (
                                    <li key={inx}>
                                    <em>
                                        <img className="lazy-image" src={ itx.pic } alt="" />
                                        <span>
                                            <img className="cf-play" src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                                        </span>
                                    </em>
                                    <h5> { itx.name }</h5>
                                    <div className="live">                                     
                                        <div className="live-play"><span className="iconfont icon-bofang2"></span>1245</div>
                                        <div className="live-time">2019-01-08</div>
                                    </div>
                                    </li>
                                )
                                })
                            }
                        </ul>
                    </div>
                    </Carousel.Item>
                )
                })
            }
            </Carousel>
      </div>
    )
  }
}

export default Com;