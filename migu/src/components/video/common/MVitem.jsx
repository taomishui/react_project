import React, { Component } from 'react';
class Com extends Component {

    goDetail (id) {
        // console.log(this.props)
        this.props.history.push('/video/mvdetails/' + id)
    }
  render () {
    //   console.log(this.props.data)
      const { pic, name, id, singer } = this.props.data;
    return (
        <li>
            <div className = "thumb" onClick = { this.goDetail.bind(this,id) }>
            <div className = "video-cover">
                <em><img src={ pic } alt=""/></em>
                <span className = "play-btn action-btn playlist-play">
                    <img src="//cdnmusic.migu.cn/v3/static/img/common/default/btn-play.png" alt=""/>
                </span>
                <div className="img-mask"></div>
            </div>
            <div className="video-name">{ name }</div>
            <div className="video-infos">
                <span className="video-info-l">
                { singer }
                </span>
            </div>
            </div>
        </li>
    )
  }
}

export default Com;