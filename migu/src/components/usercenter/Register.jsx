import React, {Component} from 'react';
import { NavLink,Link, Route, Switch, Redirect } from 'react-router-dom';
import './register.scss'
import axios from 'axios'
// import Login from './Login'
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      username: '',
      code: '',
      password: '',
      flag:'flase',
      flagtel:'flase',
      flag2:'true'
    }
  }
  // 协议
  Btn () {
    document.querySelector(".userAgreement").style.display="none";
    document.querySelector("#fade").style.display="none";
  }
  tel() {
    console.log(this.refs.username.value)
    this.setState({
      username:this.refs.username.value
    })
    // 判断手机号是否正确
    // alert(this.state.username)
    if(/^1[345678]\d{9}$/.test(this.refs.username.value)){
      // alert('tel')
      // alert(this.state.username)
      document.querySelector('#sendCode').style.backgroundColor="#e11377";
      this.setState({flagtel:'true'})
    }else{
      document.querySelector('#sendCode').style.backgroundColor="#ccc";
      this.setState({flagtel:'false'})
      // alert('error')
    }
  }
  code () {
    console.log(this.refs.code.value)
    this.setState({
      code:this.refs.code.value
    })
  }
  sendCode(){
    if(this.state.flag2=='true'&&this.state.flagtel=='true'){
      // alert('111')
      let time=10;
      this.timer=setInterval(()=>{
        this.setState({flag2:'false'})
        document.querySelector('#sendCode').innerHTML=`${time}s后继续发送`
        document.querySelector('#sendCode').style.backgroundColor="#ccc";
        time--
        if(time===-1){
          this.setState({flag2:'true'})
          document.querySelector('#sendCode').innerHTML="获取验证码"
          document.querySelector('#sendCode').style.backgroundColor="#e11377";
          clearInterval(this.timer)
        }
      },1000)
      // https://www.daxunxun.com/users/sendCode
      axios.get('https://www.daxunxun.com/users/sendCode',{
        tel:this.state.username
      })
      .then(data=>{
        console.log(data)
        // console.log(data.code)
        if(data==1){
          alert('该用户已注册')
        }else if(data==0){
          alert('发送错误')
        }else{
          alert('发送成功')
        }
      })

    }else{
      // alert('??')
    }
    
  }
  // onFocus获取焦点显示密码提示
  pw() {
    // document.querySelector("#prompt").innerHTML="请输入6-16位的数字字母或组合"
    document.querySelector("#prompt").style.display="block";  
    document.querySelector("#null").style.display="none"
    document.querySelector("#error").style.display="none"
  }
  //onBlur密码改变
  pw1() {
    console.log(this.refs.password.value)
    document.querySelector("#prompt").style.display="none";
    this.setState({
      password:this.refs.password.value
    })
    // 6-16位数字字母或符号组合
    if(this.refs.password.value==""){
      document.querySelector("#null").style.display="block"
    }else if(/^[a-zA-Z0-9_-]{6,16}$/.test(this.state.password)){
      document.querySelector("#prompt").style.display="none";
      document.querySelector("#null").style.display="none";
      this.setState({flag:true})
    }else{
      document.querySelector("#error").style.display="block"
    }
  }
  Reg(){
    if(this.state.flag==true){
      axios.post('https://www.daxunxun.com/users/register',{
      username:this.state.username,
      password:this.state.password
    })
    .then(data=>{
      console.log(data)
      if(data.data===1){
        console.log("注册成功")
        alert('注册成功')
        this.props.history.push('login')
      }else if(data.data===2){
        alert('该用户已注册')
      }
      else{
        console.log("注册失败")
        alert('注册失败')
      }
    })
    }
    else{
      alert('密码或账户格式错误')
    }
    
  }
  
  render () {
    return (
      <div>
        {/* 模态框 */}
       
        <div className="userAgreement">
          <div className="Agrtop">
            <h4>咪咕用户服务协议及隐私权政策</h4>
            <span><Link to="/culture">x</Link></span>
          </div>
          <div className="Agrcon">
            <p>在您注册咪咕产品账号的过程中，您需要完成注册流程并确认同意在线签署《咪咕用户服务协议》和《咪咕隐私权政策》（以下简称“法律文件”）。请您在确认同意之前务必仔细阅读、充分理解法律文件中的条款内容，尤其是在法律文件中以粗体或下划线标识的条款部分。</p>
            <p>如果您不同意上述法律文件或其中的任何条款约定，请您停止注册。如您按照注册流程提示填写信息、阅读并确认同意上述法律文件且完成注册流程后，即表示您已充分阅读、理解并接受上述法律文件的全部内容；并表明您同意咪咕公司可以依据法律文件约定来处理您的个人信息。</p>
            <Link to="/">《咪咕用户服务协议》</Link>
            <Link to="/">《咪咕隐私权协议》</Link>
            <button onClick={this.Btn.bind(this)}>同意并继续</button>
          </div>
        </div>
        {/* 登录界面 */}
        <div className="regirster">
          <div id="fade"></div>
          <div className="regHeader">
            <h1><img src={require("@/img/userlogo.jpg")} alt=""/></h1>
          </div>
          <div className="regCon">
            <div className="regConTop">
              <p>注册</p>
              <p>已有账号？去<Link to="/login">登录</Link></p>
              {/* <Switch>
                <Route path="/register/login" component={Login}/>
              </Switch> */}
            </div>
            <div className="regMigu">
              <h3>您正在注册咪咕账号</h3>
              <p>您可以使用该帐号登录咪咕音乐、咪咕视频、咪咕阅读、咪咕游戏、咪咕圈圈等业务。 </p>
              <p>中国移动用户已默认注册和通行证帐号，可直接登录咪咕业务。</p>
              <form action="">
                <div className="forms">
                  <label>手机号</label>
                  <input type="text" placeholder="请输入手机号" onBlur={this.tel.bind(this)} ref="username"/>
                </div>
               <div className="forms">
                  <label>短信验证码</label>
                  <input type="text" placeholder="请输入验证码" onChange={this.code.bind(this)} ref="code"/>
                  <p onClick={this.sendCode.bind(this)} id="sendCode">获取验证码</p>
               </div>
                <div className="forms">
                  <label>设置密码</label>
                  {/* <input type="text" class="txt J_DelectIcon Validform_error" value="" autocomplete="off" placeholder="请输入密码" id="J_NewPassword" maxlength="16" datatype="psdvalid" nullmsg="密码不能为空" errormsg="请输入6-16位数字、字母或符号的组合" cmcc-psdid="J_Eye_DAD44A3C14A245899DE51BF51E2E26B9" ignore="ignore" cmcc-type="password" /> */}
                  <input type="password" placeholder="请输入密码" onFocus={this.pw.bind(this)} onBlur={this.pw1.bind(this)} ref="password"/>
                  <p>
                    <span id="prompt">请输入6-16位的数字字母或组合</span>
                    <span id="null">密码不能为空</span>
                    <span id="error">密码格式错误</span>
                  </p>
                </div>
                <div className="forms">
                  <label></label>
                  <input type="button" value="立即注册" className="regbutton" onClick={this.Reg.bind(this)}/>
                </div>
              </form>
            </div>
          </div>
          <div className="regFooter">
            <ul>
              <li>公司简介</li>
              <li>帮助中心</li>
              <li>用户指南</li>
              <li>移动门户</li>
              <li>咪咕用户服务协议</li>
              <li>咪咕隐私政策</li>
              <li>蜀ICP备15012512号-1 © 2018 咪咕文化科技有限公司</li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
export default Com;