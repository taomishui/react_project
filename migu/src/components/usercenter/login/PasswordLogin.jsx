import React, {Component} from 'react';
import { Link, NavLink } from 'react-router-dom';
import axios from 'axios'
import {
  Form, Icon, Input, Button, Checkbox,
} from 'antd';
function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
class NormalLoginForm extends Component {
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          console.log(values.userName)
          console.log(values.password)
          axios.post('https://www.daxunxun.com/users/login',{
            username:values.userName,
            password:values.password
          })
          .then(data=>{
            console.log(data)
            if(data.data==1){
              alert('登录成功')
              localStorage.setItem('isLogin', 'ok')
              localStorage.setItem('name', values.userName)
              this.props.history.push('/user')
            }else if(data.data==0){
              alert('登录失败')
            }else if(data.data==2){
              alert('没有该用户')
            }else{
              alert('密码错误')
            }
          })
        }
      });
    }
    

    render() {
      const { getFieldDecorator } = this.props.form;
      return (
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item>
            {getFieldDecorator('userName', {
              rules: [{ required: true, message: '请输入账号!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="手机号/邮箱/用户名/和通行证" />
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: '请输入密码!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="密码"/>
            )}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              登录
            </Button>
            {getFieldDecorator('remember', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox>记住密码</Checkbox>
            )}
            <a className="login-form-forgot" href="">忘记密码?</a>
            
            {/* Or <a href="">register now!</a> */}
          </Form.Item>
        </Form>
      );
    }
  }
  const WrappedNormalLoginForm = Form.create()(NormalLoginForm);
  export default WrappedNormalLoginForm;