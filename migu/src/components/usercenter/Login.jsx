import React, {Component} from 'react';
import { Link, NavLink, Route, Switch, Redirect } from 'react-router-dom';
import './login.scss';
import SignIn from './login/SignIn'
import PwLogin from './login/PasswordLogin'
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={}
  }
  quxiao () {
    // document.querySelector(".loginform").style.display="none";
  }
  render () {
    return (
      <div>
        <div className="login">
          <div id="fade"></div>
          <div className="loginform">
            <div className="logintop">
              <p><NavLink to="/login/pw">密码登录</NavLink></p>
              <p><NavLink to="/login/sign">短信登录</NavLink></p>
              <span onClick={this.quxiao.bind(this)}>X</span>
            </div>
            <Switch>
              <Route path="/login/sign" component={SignIn}/>
              <Route path="/login/pw" component={PwLogin}/>
              <Redirect from='/login' to='/login/pw' />
            </Switch>
            <div className="loginbottom">
              <p>没有账号？<Link to="/register">注册</Link></p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Com;