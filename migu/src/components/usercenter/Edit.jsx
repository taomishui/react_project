import React, {Component} from 'react';
import { Link, Route, Switch, Redirect, NavLink } from 'react-router-dom';
import './edit.scss';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:localStorage.getItem('name')
    }
  }
  logo(){
    this.props.history.push('/')
    alert('111')
  }
  exit(){
    localStorage.clear()
  }
  render () {
    return (
      <div>
        <div className="userheader">
          <h1 onClick={this.logo.bind(this)}><img src={require("@/img/userlogo.jpg")} alt=""/></h1>
          <ul>
            <li>
              <p>欢迎，{this.state.name}</p>
            </li>
            <li>
              <Link to="/culture" onClick={this.exit.bind(this)}>退出</Link>
            </li>
          </ul>
        </div>
        <div className="editcon">
          <div className="edit_left">
            <div className="jibenmsg">
              <div className="msgtop">
                <h3>基本信息</h3>
              </div>
              <div className="msgitem">
                <ul>
                  <li><NavLink to="/">账号安全</NavLink></li>
                  <li><NavLink to="/1">个人资料</NavLink></li>
                  <li><NavLink to="/2">账号绑定</NavLink></li>
                </ul>
              </div>
            </div>
            <div className="alipay">
              <div className="msgtop">
                <h3>支付账号</h3>
              </div>
              <div className="msgitem">
                <ul>
                  <li><NavLink to="/1">支付密码</NavLink></li>
                  <li><NavLink to="/2">账号详情</NavLink></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="edit_right">
            <h3 class="title">帐号安全</h3>
            <div class="setsafe">
              <dl>
                <dt>
                  <em class="active"></em>
                  <span>绑定手机</span>
                </dt>
                <dd>
                  <span>您已绑定手机号：{this.state.name}</span>
                  <a href="">修改</a>
                </dd>
              </dl>						
              <dl>
                <dt>
                  <em class="active"></em>
                  <span>密码强度</span>
                </dt>
                <dd>
                  <span>密码强度为：中</span>
                  <a href="">修改</a>
                </dd>
              </dl>
              <dl className="nobd">
                <dt>
                  <em></em>
                  <span>密保问题</span>
                </dt>
                <dd>
                  <span>您未设置密保问题</span>
                  <a href="">立即设置</a>
                </dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Com;