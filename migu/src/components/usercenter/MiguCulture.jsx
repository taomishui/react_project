import React, {Component} from 'react';
import { Link, Route, Switch, Redirect, NavLink } from 'react-router-dom';
import './usercenter.scss';
import Guide from './miguculture/Guide';
import AccountNum from './miguculture/AccountNum';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={}
  }
  render () {
    return (
      <div>
        <div className="userheader">
          <h1><img src={require("@/img/userlogo.jpg")} alt=""/></h1>
          <ul>
            <li>
              <Link to="/register">注册</Link>
            </li>
            <li>
              <Link to="/login">登录</Link>
            </li>
          </ul>
        </div>
        <div className="usercontent">
          <div className="usercontitle">
            <ul>
              <li><NavLink to="/culture/accountNum">账号</NavLink></li>
              <li><NavLink to="/culture/guide">指南</NavLink></li>
            </ul>
          </div>
        </div>
        <Switch>
          <Route path="/culture/guide" component={Guide} />
          <Route path="/culture/accountNum" component={AccountNum} />
          <Redirect from="/culture" to="/culture/accountNum" />
        </Switch>
      </div>
    )
  }
}
export default Com;