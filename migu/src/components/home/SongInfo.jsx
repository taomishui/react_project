import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import './songInfo.scss';
import axios from 'axios';
// import '@/mock/music';
class Com extends Component{
  constructor(props){
    super(props);
    this.state={
      value:'',
      time:''
    }
  }
  isLogin(){
    if(localStorage.getItem('isLogin') !== 'ok'){
      alert('请登录后评论哦~')
      this.props.history.push('/login')
    }else{

    }
  }
  comment(){
    console.log(this.refs.comment.value)
    this.setState({
      value:this.refs.comment.value
    })
  }
  btn(){
    // 获取当前时间
    var oDate=new Date()
    // console.log(oDate.getFullYear()+"-"+oDate.getMonth()+"-"+oDate.getDate(), oDate.getHours()+":"+oDate.getMinutes()+":"+oDate.getSeconds())
    var time=oDate.getFullYear()+"-"+oDate.getMonth()+"-"+oDate.getDate()+"&nbsp;"+oDate.getHours()+":"+oDate.getMinutes()+":"+oDate.getSeconds();
    // console.log(11+time)
    this.setState({
      time:time
    })
    var aDiv=document.createElement('div');
    aDiv.className="commentItem"
    document.querySelector(".comments").appendChild(aDiv);
    // 评论 p
    var aP=document.createElement('p');
    aP.innerHTML="我的评论："+this.state.value;
    document.querySelector(".commentItem").appendChild(aP);
    // 时间 span
    var aSpan=document.createElement('span');
    aSpan.innerHTML=time
    document.querySelector(".commentItem").appendChild(aSpan);
    alert('发表评论成功，审核后通过后才能看到你的评论哦~')
  }
  conponentDidMount(){

  }
  render(){
    return(
      <div className="de_contain">
        <div className="contain_song">
          <div className="img_contain">
            <img src="http://cdnmusic.migu.cn/picture/2018/0622/0849/AS4aee154140874aa39ff991f1a158e967.jpg" alt=""/>
          </div>
          <div className="info_contain">
            <h2>纸短情长</h2>
            <h5><i className="iconfont icon-ren"></i>花粥</h5>
            <p>作词：言寺</p>
            <p>作曲：言寺</p>
            <p>所属专辑：纸短情长</p>
            <p>标签：流行 爱情 思念 国语 翻唱 驾车</p>
            <div className="more_contain">
            <button><i className="iconfont icon-bofang1"></i>播放全部</button>
            <button><i className="iconfont icon-lingsheng"></i>彩铃订购</button>
            <button><i className="iconfont icon-xiazai"></i>下载</button>
            <button>…更多</button>
          </div>
          </div>
          
        </div>
        <div className="contain_lyric">
          <h2>歌词</h2>
          <p class="lyric-text">作曲：言寺
          </p>
          <p class="lyric-text">你陪我步入蝉夏越过城市喧嚣
          </p>
          <p class="lyric-text">歌声还在游走你榴花般的双眸
          </p>
          <p class="lyric-text">不见你的温柔丢失花间欢笑
          </p>
          <p class="lyric-text">岁月无法停留流云的等候
          </p>
          <p class="lyric-text">我真的好想你在每一个雨季
          </p>
          <p class="lyric-text">你选择遗忘的是我最不舍的
          </p>
          <p class="lyric-text">纸短情长啊道不尽太多涟漪
          </p>
          <p class="lyric-text">我的故事都是关于你呀
          </p>
          <p class="lyric-text">怎么会爱上了他并决定跟他回家
          </p>
          <p class="lyric-text">放弃了我的所有我的一切无所谓
          </p>
          <p class="lyric-text">纸短情长啊诉不完当时年少
          </p>
          <p class="lyric-text">我的故事还是关于你啊
          </p>
          <p class="lyric-text">你陪我步入蝉夏越过城市喧嚣
          </p>
          <p class="lyric-text">歌声还在游走你榴花般的双眸
          </p>
          <p class="lyric-text">不见你的温柔丢失花间欢笑
          </p>
          <p class="lyric-text">岁月无法停留流云的等候
          </p>
          <p class="lyric-text">我真的好想你在每一个雨季
          </p>
          <p class="lyric-text">你选择遗忘的是我最不舍的
          </p>
          <p class="lyric-text">纸短情长啊道不尽太多涟漪
          </p>
          <p class="lyric-text">我的故事都是关于你呀
          </p>
          <p class="lyric-text">怎么会爱上了他并决定跟他回家
          </p>
          <p class="lyric-text">放弃了我的所有我的一切无所谓
          </p>
          <p class="lyric-text">纸短情长啊诉不完当时年少
          </p>
          <p class="lyric-text">我的故事还是关于你啊
          </p>
          <p class="lyric-text">你陪我步入蝉夏越过城市喧嚣
          </p>
          <p class="lyric-text">歌声还在游走你榴花般的双眸
          </p>
          <p class="lyric-text">不见你的温柔丢失花间欢笑
          </p>
          <p class="lyric-text">岁月无法停留流云的等候
          </p>
          <span>展开</span>
        </div>
        <div className="contain_like">
          <h2>相似歌曲</h2>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
          <div className="item-box">
            <dl>
              <dt>
                <img src="http://cdnmusic.migu.cn/picture/2017/1109/0043/AS123e0dc996df4c42871a6d441343eff4.jpg" alt=""/>
                <b className="iconfont icon-bofang"></b>
              </dt>
              <dd>
                <p>十年十年十年十年十年十年十年十年十年十年十年十年</p>
                <span>陈奕迅</span>
              </dd>
              <dd>03:13</dd>
              <dd>
                <i className="iconfont icon-xin"></i>
                <i className="iconfont icon-share"></i>
                <i className="iconfont icon-lingsheng"></i>
              </dd>
            </dl>
          </div>
        </div>
        <div className="contain_comment">
          <h2>用户评论</h2>
          <div className="comment_form">
            <textarea id="comment" name="commentBody" placeholder="快发表精彩评论抢沙发" ref="comment"onBlur={this.comment.bind(this)} onFocus={this.isLogin.bind(this)}></textarea>
            <span>0/80</span>
          </div>
          <div className="comment_btn">
            <button id="btn" onClick={this.btn.bind(this)}>发表评论</button>
          </div>
          <div className="comments">
          </div>
        </div>
      </div>
    )
  }
}
export default Com;