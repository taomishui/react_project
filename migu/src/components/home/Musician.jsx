import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import './musician.scss';
import axios from 'axios';
import '@/mock/music';
class Com extends Component{
  constructor(props){
    super(props);
    this.state={
      list:[]
    }
  }
  componentDidMount(){
    axios.get('http://www.zz1807.com/musician')
    .then(data=>{
      console.log(data.data.list)
      this.setState({
        list:data.data.list
      })
    })
  }
  render(){
    return(
      <div>
        <div className="music_title">
          <ul>
            <li><NavLink to="/">全部</NavLink></li>
            <li><NavLink to="/1">华语男歌手</NavLink></li>
            <li><NavLink to="/2">咪咕音乐人</NavLink></li>
            <li><NavLink to="/3">华语组合</NavLink></li>
            <li><NavLink to="/4">华语女歌手</NavLink></li>
            <li><NavLink to="/5">欧美男歌手</NavLink></li>
            <li><NavLink to="/6">欧美女歌手</NavLink></li>
            <li><NavLink to="/7">欧美组合</NavLink></li>
            <li><NavLink to="/8">日韩男歌手</NavLink></li>
            <li><NavLink to="/9">日韩女歌手</NavLink></li>
            <li><NavLink to="/0">日韩组合</NavLink></li>
          </ul>
        </div>
        <div className="musicians">
          <ul>
            {
              this.state.list.map((item,index)=>{
                return(
                  <li key={index}>
                    <img src={item.img} alt=""/>
                    <p>{item.singer}</p>
                  </li>
                )
              })
            }
          </ul>
        </div>
      </div>
    )
  }
}
export default Com;