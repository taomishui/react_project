import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import './singer.scss';

import axios from 'axios';
import Swiper from 'swiper'
import '@/dist/css/swiper.min.css'
import '@/mock/music';
class Com extends Component{
  constructor(props){
    super(props);
    this.state={
      list:[]
    }
  }
  componentDidMount() {
    var mySwiper = new Swiper('.swiper-container', {
      direction:'horizontal',
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      }
    })
  }
  render(){
    return(
      <div>
        <div className="singerheader">
          <h1><img src={require('@/img/logo.png')} alt=""/></h1>
          <h2>咪咕音乐人</h2>
          <ul className="singerindex">
            <li><NavLink to="/">首页</NavLink></li>
          </ul>
          <div className="singer_r">
            <p><NavLink to="/1">成为音乐人</NavLink></p>
            <p><NavLink to="/2">音乐人登录</NavLink></p>
          </div>
          <div className="sousuo">
            <input type="text" placeholder="搜索歌曲、歌手、MV"/>
            <span class="btn-search"><i class="iconfont icon-sousuo"></i></span>
          </div>
        </div>
        <div className="singerbanner">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src="https://cdnmusic.migu.cn/cmsupload/12003/upload/graphicLink/20190108091707178219.png" alt=""/>
            </div>
            <div class="swiper-slide">
              <img src="https://cdnmusic.migu.cn/cmsupload/12003/upload/graphicLink/20190107142800281688.jpg" alt=""/>
            </div>
            <div class="swiper-slide">
              <img src="https://cdnmusic.migu.cn/cmsupload/12003/upload/graphicLink/20190107142744276456.jpg" alt=""/>
            </div>
            <div className="swiper-slide">
            <img src="https://cdnmusic.migu.cn/cmsupload/12003/upload/graphicLink/20190102092359231837.jpg" alt=""/></div>
          </div>
          <div class="swiper-pagination"></div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
        </div>
        <div className="singercon">
        {/* 最新单曲 */}
          <div> 
            <div className="title">
              最新单曲
              <Link to="/">更多
              <i className="iconfont icon-jiantouyou"></i></Link>
            </div>
            <div className="Lastsingle">
              <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div>
                    <img src="http://cdnmusic.migu.cn/picture/2019/0109/1156/ASa106244a66c245a0aef1ddce132255db.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>若只如初见</p>
                      <span>李若溪</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2019/0108/0935/AS9c9a0f8e44464d88829868e0e7aa2eac.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>Transparent</p>
                      <span>Cephalosis</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://d.musicapp.migu.cn/ugcdata/playListimg/aea96f1f-c214-43b9-a78d-f28fdc3e4ccf.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>最佳抖音</p>
                      <span>艾丁格</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2017/1109/0814/AMad7abbbc204d4d7f915a4db8da1d0bf3.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>本色十年</p>
                      <span>左右乐队</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                </div>
                <div class="swiper-slide">
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
          {/* 原创单曲 */}
          <div>
            <div className="title">
            原创单曲
              <Link to="/">更多
              <i className="iconfont icon-jiantouyou"></i></Link>
            </div>
            <div className="Originalalbum">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div>
                    <img src="http://cdnmusic.migu.cn/picture/2019/0109/1156/ASa106244a66c245a0aef1ddce132255db.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>若只如初见</p>
                      <span>李若溪</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2019/0108/0935/AS9c9a0f8e44464d88829868e0e7aa2eac.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>Transparent</p>
                      <span>Cephalosis</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://d.musicapp.migu.cn/ugcdata/playListimg/aea96f1f-c214-43b9-a78d-f28fdc3e4ccf.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>最佳抖音</p>
                      <span>艾丁格</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2017/1109/0814/AMad7abbbc204d4d7f915a4db8da1d0bf3.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>本色十年</p>
                      <span>左右乐队</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                </div>
                <div class="swiper-slide">
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
          {/* 特别企划 */}
          <div> 
            <div className="title">
            特别企划
              <Link to="/">更多
              <i className="iconfont icon-jiantouyou"></i></Link>
            </div>
            <div className="Specialreport">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div>
                    <img src="http://cdnmusic.migu.cn/picture/2019/0109/1156/ASa106244a66c245a0aef1ddce132255db.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>若只如初见</p>
                      <span>李若溪</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2019/0108/0935/AS9c9a0f8e44464d88829868e0e7aa2eac.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>Transparent</p>
                      <span>Cephalosis</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://d.musicapp.migu.cn/ugcdata/playListimg/aea96f1f-c214-43b9-a78d-f28fdc3e4ccf.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>最佳抖音</p>
                      <span>艾丁格</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2017/1109/0814/AMad7abbbc204d4d7f915a4db8da1d0bf3.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>本色十年</p>
                      <span>左右乐队</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                </div>
                <div class="swiper-slide">
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                  <div><img src={require("@/img/img/3.jpg")} alt=""/></div>
                </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
          {/* 入住音乐人 */}
          <div> 
            <div className="title">
            入住音乐人
              <Link to="/">更多
              <i className="iconfont icon-jiantouyou"></i></Link>
            </div>
            <div className="Musicianin">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div>
                    <img src="http://cdnmusic.migu.cn/picture/2019/0109/1156/ASa106244a66c245a0aef1ddce132255db.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>若只如初见</p>
                      <span>李若溪</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2019/0108/0935/AS9c9a0f8e44464d88829868e0e7aa2eac.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>Transparent</p>
                      <span>Cephalosis</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://d.musicapp.migu.cn/ugcdata/playListimg/aea96f1f-c214-43b9-a78d-f28fdc3e4ccf.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>最佳抖音</p>
                      <span>艾丁格</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2017/1109/0814/AMad7abbbc204d4d7f915a4db8da1d0bf3.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>本色十年</p>
                      <span>左右乐队</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
          {/* 最in视频 */}
          <div> 
            <div className="title">
            最in视频
              <Link to="/">更多
              <i className="iconfont icon-jiantouyou"></i></Link>
            </div>
            <div className="Mostin">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div>
                    <img src="http://cdnmusic.migu.cn/picture/2019/0109/1156/ASa106244a66c245a0aef1ddce132255db.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>若只如初见</p>
                      <span>李若溪</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2019/0108/0935/AS9c9a0f8e44464d88829868e0e7aa2eac.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>Transparent</p>
                      <span>Cephalosis</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl>
                  </div>
                  <div><img src="http://d.musicapp.migu.cn/ugcdata/playListimg/aea96f1f-c214-43b9-a78d-f28fdc3e4ccf.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>最佳抖音</p>
                      <span>艾丁格</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                  <div><img src="http://cdnmusic.migu.cn/picture/2017/1109/0814/AMad7abbbc204d4d7f915a4db8da1d0bf3.jpg" alt=""/>
                  <dl>
                    <dt>
                      <p>本色十年</p>
                      <span>左右乐队</span>
                    </dt>
                    <dd><i className="iconfont icon-fenxiang1"></i></dd>
                  </dl></div>
                </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default Com;