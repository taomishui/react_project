import React, {Component} from 'react';
import { NavLink,Link, Route, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';
import '@/mock/mylisten';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      list:[],
      chooseCheck:0,
      Allchoose:true, // 全选
      checkitem:true,
      isAuto:true
    }
  }

  checkitem () {
    this.setState({
      checkitem:false
    })
  }  
  Allchoose(e){
    if(this.state.Allchoose){
        this.setState({
            chooseCheck : 0,
            Allchoose : false,
            isAuto : false,
        })
    }else{
        this.setState({
            Allchoose : true,
            chooseCheck : this.state.chooseList.length,
            isAuto : false,
        });
    }
}
checkAllChoose(ItemChecked){
  if(this.state.isAuto = true)this.state.isAuto = false;
  ItemChecked ? this.state.chooseCheck++ : this.state.chooseCheck--;
  console.log(this.state.chooseCheck);
  if(this.state.chooseCheck == this.state.chooseList.length){
      this.setState({
          Allchoose : true,
          isAuto : true
      })
  }else{
      if(this.state.Allchoose == true){
          this.setState({
              Allchoose : false,
              isAuto : true
          });
      }
  }
}
_clickHandle(e){
  this.props.checkAllChoose(!this.state.checked);
  this.setState({
      checked : this.state.checked ? false : true
  });
}
componentWillReceiveProps(nextprops){
  console.log(nextprops.isAuto);
  if(nextprops.isAuto == false)this.state.checked = nextprops.Allchoose;
  if(nextprops.Reverse != this.props.Reverse){
      this.setState({
          checked : !this.state.checked
      })
  }
}
  componentDidMount(){
    axios.get('http://www.zz1807.com/mylisten')
    .then(data=>{
      console.log(data.data.list)
      this.setState({
        list:data.data.list
      })
    })
  }
  render (){
    return (
      <div className="my_check">
        <div className="recent_listen">
          <ul>
            {
              this.state.list.map((item,index)=>{
                return(
                  <li key={index}>
                  <input type="checkbox" value = {this.props.value} checked = {this.props.Allchoose ? this.props.Allchoose : this.state.checked} onClick = {(e)=>{this._clickHandle(e)}}/>
                    <i>{item.id}</i>
                    <p>{item.name}</p>
                    <span>{item.singer}</span>
                  </li>
                )
              })
            }
            
          </ul>
        </div>
        <div className="check_action">
          <input type="checkbox" name="" id="allcheck" onClick = {(e)=>{this.Allchoose(e)}} checked = {this.state.Allchoose}/>
          <span>全选</span>
          <button>播放选择</button>
        </div>
      </div>
    )
  }
}

export default Com;