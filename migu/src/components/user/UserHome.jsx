import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MyCollect from './MyCollect';
import Subscribe from './Subscribe';
import MyListen from './MyListen'
import './user.scss'
class Com extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:localStorage.getItem('name')
    }
  }
  render() {
    return (
      <div className="container">
        <div className="Information">
          <dl>
            <dt><img src={require("@/img/default-img-120x120.png")} alt="" /></dt>
            <dd>
              <div className="name">
                <h4>{this.state.name}</h4>
                <span><Link to="/edit">编辑</Link></span>
              </div>
              <div className="vip">
                <label>非会员</label>
                <span><Link to="/">开通白金会员</Link></span>
              </div>
              <div className="integral">
                <label>积分：</label>
                <p><span>0/20</span></p>
                <p>LV.1</p>
                <p><Link to="/">成长攻略</Link></p>
              </div>
              <div className="userstat">
                <p>歌单</p><span>0</span>
                <p>粉丝</p><span>0</span>
                <p>好友</p><span>0</span>
              </div>
            </dd>
          </dl>
          <div className="bells">
            <p>我的铃声库</p>
          </div>
        </div>
        <div className="MyListen">
          <div className="my_title">
            <h2>我最近在听</h2>
          </div>
          <MyListen/>
        </div>
        <Subscribe />
        <MyCollect />
        <div className="MySongList">
          <div className="my_title">
            <h2>我的歌单</h2>
          </div>
          <div className="collect_con">
            <img src={require("@/img/empty_error.png")} alt="" />
            <p>您的歌单太少了，快去<Link to="/">音乐频道</Link>将喜欢的歌做成自己的歌单吧！</p>
          </div>
        </div>
        <div className="MyVideo">
          <div className="my_title">
            <h2>我的视频</h2>
          </div>
          <div className="collect_con">
            <img src={require("@/img/empty_error.png")} alt="" />
            <p>你还没有分享您的精彩视频哦~现在去<Link to="/">上传</Link>吧！</p>
          </div>
        </div>
        <div className="DigitalAlbum">
          <div className="my_title">
            <h2>数字专辑</h2>
          </div>
          <div className="collect_con">
            <img src={require("@/img/empty_error.png")} alt="" />
            <p>您还没有购买过数字专辑，快去<Link to="/">数字专辑</Link>频道购买喜欢的专辑吧！</p>
          </div>
        </div>
        <div className="MyBell">
          <div className="my_title">
            <h2>我的铃声库</h2>
            <span>更多彩铃功能，请到<Link to="/">咪咕音乐APP</Link>上使用！</span>
          </div>
          <div className="collect_con">
            <img src={require("@/img/empty_error.png")} alt="" />
            <p>你还没有定制自己的彩铃，快去<Link to="/">音乐频道</Link>将喜欢的歌曲定制为彩铃吧！</p>
          </div>
        </div>
      </div>
    )
  }
}
export default Com;