import React, {Component} from 'react';
import { NavLink,Link, Route, Switch, Redirect } from 'react-router-dom';
import axios from 'axios';
import '@/mock/mylisten';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      list:[],
      chooseCheck:0,
      Allchoose:true, // 全选
      checkitem:true,
      isAuto:true
    }
  }
// 全选 
checkall(){
  if(this.state.Allchoose==true){
    this.setState({
      Allchoose:false,
      checkitem:false,
      chooseCheck:0,
      isAuto:true
    })
  }else{
    this.setState({
      Allchoose:true,
      checkitem:true,
      chooseCheck:this.state.list.length,
      isAuto:false
    })
  }
    // this.setState({
    //   Allchoose:this.state.Allchoose?false:true
    // })

  }
// 单选
_clickHandle(e){
  console.log(e.currentTarget.checked)
  e.currentTarget.checked=!e.currentTarget.checked
  this.setState({
    // checkitem:this.state.checkitem?false:true
    // Allchoose:false
    
  })
}
// componentWillReceiveProps(nextProps,prevProps){
//   if(nextProps.checkitem != prevProps.checkitem){
//     this.setState({
//       checkitem: nextProps.checkitem
//     })
//   }
// }
  componentDidMount(){
    axios.get('http://www.zz1807.com/mylisten')
    .then(data=>{
      console.log(data.data.list)
      this.setState({
        list:data.data.list
      })
    })
  }

  componentDidUpdate(){
    if(this.state.list.length!==0&&localStorage.getItem('isLogin') === 'ok'){
      document.querySelector('.collect_con').style.display='none'
      document.querySelector('.my_check').style.display='block'
      
    }else{
      document.querySelector('.collect_con').style.display='block'
      document.querySelector('.my_check').style.display='none'
    }
  }
  render (){
    return (
      <div className="my_check">
        <div className="recent_listen">
          <ul>
            {
              this.state.list.map((item,index)=>{
                return(
                  <li key={index}>
                  <input type="checkbox" value = {item.id} checked = {this.state.checkitem} onClick = {(e)=>{this._clickHandle(e)}}/>
                    <i>{item.id}</i>
                    <p>{item.name}</p>
                    <span>{item.singer}</span>
                  </li>
                )
              })
            }
          </ul>
        </div>
        <div className="check_action">
          <input type="checkbox" name="" id="allcheck" onClick = {this.checkall.bind(this)} checked = {this.state.Allchoose}/>
          <span>全选</span>
          <button className="btn">播放选择</button>
        </div>
      </div>
    )
  }
}

export default Com;