import React, {Component} from 'react';
import { Link, Route, Switch, Redirect } from 'react-router-dom';
import CollectKind from './collect/CollectKind'
// import CollectCon from './collect/CollectCon'
import Song from './collect/collect_con/Song'
import SongList from './collect/collect_con/SongList'
import Singer from './collect/collect_con/Singer'
import Album from './collect/collect_con/Album'
import Video from './collect/collect_con/Video'
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      list: [
        {
          path:'/user/song',
          name:'歌曲'
        },
        {
          path:'/user/songlist',
          name:'歌单'
        },
        {
          path:'/user/album',
          name:'专辑'
        },
        {
          path:'/user/video',
          name:'视频'
        },
        {
          path:'/user/singer',
          name:'歌手'
        }
      ]
    }
  }
  render () {
    return (
      <div className="MyCollect">
        <div className="my_title">
          <h2>我的收藏</h2>
        </div>
        <CollectKind 
          list={this.state.list}
        />
        <Switch>
          <Route path="/user/songlist" component={SongList} />
          <Route path="/user/song" component={Song} />
          <Route path="/user/singer" component={Singer} />
          <Route path="/user/video" component={Video} />
          <Route path="/user/album" component={Album} />
          <Redirect from="/user" to="/user/song" />
        </Switch>
      </div>
    )
  }
}
export default Com;