import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '@/mock/mylisten';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      list:[]
    }
  }
  componentDidMount(){
    axios.get('http://www.zz1807.com/mylisten')
    .then(data=>{
      console.log(data.data.list)
      this.setState({
        list:data.data.list
      })
    })
  }
  componentDidUpdate(){
    if(this.state.list.length!==0&&localStorage.getItem('isLogin') === 'ok'){
      document.querySelector('.collect_con').style.display='none'
      document.querySelector('.havecollect').style.display='block'
      
    }else{
      document.querySelector('.collect_con').style.display='block'
      document.querySelector('.havecollect').style.display='none'
    }
  }
  render (){
    return(
      <div>
      <div className="collect_con">
        <img src={require("@/img/empty_error.png")} alt=""/>
        <p>您还没有收藏歌曲，快去<Link to="/">音乐频道</Link>收藏喜欢的歌曲吧!</p>
      </div>
      <div className="havecollect">
      <ul>
        {
          this.state.list.map((item,index)=>{
            return(
              <li key={index}>
              <input type="checkbox" />
                <i>{item.id}</i>
                <p>{item.name}</p>
                <span>{item.singer}</span>
              </li>
            )
          })
        }
      </ul>
      <div className="check_action">
          <input type="checkbox" name="" />
          <span>全选</span>
          <button className="btn">播放选择</button>
        </div>
    </div>
    </div>
    )
  }
}
export default Com;