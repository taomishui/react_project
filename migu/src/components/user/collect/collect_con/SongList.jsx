import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '@/mock/mycollect';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={
      list:[]
    }
  }
  componentDidMount(){
    axios.get('http://www.zz1807.com/Singlist')
    .then(data=>{
      console.log(data.data.list)
      this.setState({
        list:data.data.list
      })
    })
  }
  componentDidUpdate(){
    if(this.state.list.length!==0&&localStorage.getItem('isLogin') === 'ok'){
      document.querySelector('.collect_con').style.display='none'
      document.querySelector('.collects').style.display='block'
      
    }else{
      document.querySelector('.collect_con').style.display='block'
      document.querySelector('.collects').style.display='none'
    }
  }
  render (){
    return(
      <div>
      <div className="collect_con" style={{display:'none'}}>
        <img src={require("@/img/empty_error.png")} alt=""/>
        <p>您还没有收藏歌单，快去<Link to="/">音乐频道</Link>收藏喜欢的歌单吧!</p>
      </div>
      <div className="collects">
        <ul>
          {
            this.state.list.map((item,index)=>{
              return(
                <li key={index}>
                <input type="checkbox"/>
                  <img src={item.img} alt=""/>
                  <p>{item.title}</p>
                  <span>{item.brief}</span>
                  <i className="iconfont icon-headset"> {item.num}</i>
                </li>
              )
            })
          }
        </ul>
        <button className="btn">取消收藏</button>
      </div>
      </div>
    )
  }
}
export default Com;