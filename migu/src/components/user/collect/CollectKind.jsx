import React, {Component} from 'react';
import { Link,NavLink } from 'react-router-dom';
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={}
  }
  render (){
    return(
      <div className="collect_kind">
        <ul>
          {
            this.props.list.map((item, index) => {
              return(
                <li key={index}>
                  <NavLink to={item.path}>{item.name}<b></b></NavLink>
                </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}
export default Com;