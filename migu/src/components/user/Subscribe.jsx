import React, {Component} from 'react';
import Swiper from 'swiper'
import { Link } from 'react-router-dom';
import './subscribe.scss'
// import '@/dist/js/swiper.min'
import '@/dist/css/swiper.min.css'
class Com extends Component {
  constructor(props) {
    super(props);
    this.state={}
  }

  componentDidMount() {
    var mySwiper = new Swiper('.swiper-container', {
      direction:'horizontal',
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      }
    })
    // document.querySelector('.swiper-slide')
  }
  render () {
    return (
      <div className="subscribe">
        <div className="my_title">
          <h2>我的订阅</h2>
        </div>
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <img src={require("@/img/img/1.jpg")} alt=""/>
              <img src={require("@/img/img/2.jpg")} alt=""/>
              <img src={require("@/img/img/3.jpg")} alt=""/>
              <img src={require("@/img/img/4.jpg")} alt=""/>
            </div>
            <div class="swiper-slide">
              <img src={require("@/img/img/1.jpg")} alt=""/>
              <img src={require("@/img/img/2.jpg")} alt=""/>
              <img src={require("@/img/img/3.jpg")} alt=""/>
              <img src={require("@/img/img/4.jpg")} alt=""/>
            </div>
          </div>
          <div class="swiper-pagination"></div>
          <div class="swiper-button-next"></div>
          <div class="swiper-button-prev"></div>
        </div>
        {/* <p class="append-buttons">
          <a href="#" class="prepend-2-slides">Prepend 2 Slides</a>
          <a href="#" class="prepend-slide">Prepend Slide</a>
          <a href="#" class="append-slide">Append Slide</a>
          <a href="#" class="append-2-slides">Append 2 Slides</a>
        </p> */}
      </div>
    )
  }
}
export default Com;