import Mock from 'mockjs';
var arrAlbum = []
var arrSinglist = []
var arrVidio = []
var arrSinger = []
// 专辑
for (var i = 0; i < 5; i++ ) {
	var obj = {
		// 'id': i + 1,
		// 'name': Mock.mock('@csentence'),
		'boolean':true,
		'title': Mock.mock('@ctitle'),
		'img': Mock.Random.image('225x225', '#603F38', '#FFF', 'png', '!'),
    'singer':Mock.mock('@cname'),
    'time':Mock.mock('@date')
		// 'stock|500-1000': 1,
		// 'sales|1-50': 1
	}
	arrAlbum.push(obj)
}
Mock.mock('http://www.zz1807.com/Album', {
	list: arrAlbum
})
// 歌单
for (var i = 0; i < 5; i++ ) {
	var obj = {
		'boolean':true,
		'title': Mock.mock('@ctitle'),
		'img': Mock.Random.image('225x225', '#894FC4', '#FFF', 'png', '!'),
    'singer':Mock.mock('@cname'),
    'brief':Mock.mock('@csentence'),
    'num|1-10000':1
	}
	arrSinglist.push(obj)
}
Mock.mock('http://www.zz1807.com/Singlist', {
	list: arrSinglist
})
// 视频
for (var i = 0; i < 3; i++ ) {
	var obj = {
		'boolean':true,
		'title': Mock.mock('@ctitle'),
		'img': Mock.Random.image('285x192', '#587C0C', '#FFF', 'png', '!'),
    'name':Mock.mock('@cname'),
    'num|1-10000':1
	}
	arrVidio.push(obj)
}
Mock.mock('http://www.zz1807.com/Vidio', {
	list: arrVidio
})
// 歌手
for (var i = 0; i < 5; i++ ) {
	var obj = {
		'boolean':true,
		'img': Mock.Random.image('225x225', '#EDE914', '#FFF', 'png', '!'),
    'singer':Mock.mock('@cname')
	}
	arrSinger.push(obj)
}
Mock.mock('http://www.zz1807.com/Singer', {
	list: arrSinger
})
 