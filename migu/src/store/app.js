const reducer = (state ={
  list: []
},action)=>{
  const {type, data} = action;
  switch(type) {
    case 'CHANGE_ITEM':
      state.list = data
      return state;
    default:
      return state;
  }
}

export default reducer;