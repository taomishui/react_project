import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import appdata from './app';
import mvdata from '@/components/video/store';

//combineReducers 组合各个reducer，每个reducer可以看做是一个单独的状态管理器
const reducer = combineReducers({
   app:appdata,
   mv: mvdata
})

const store = createStore(reducer, applyMiddleware(thunk));
export default store;