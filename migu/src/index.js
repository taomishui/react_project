import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import App from '@/App';
import User from '@/components/user/UserHome';
import Video from '@/components/video/Video';
import Register from '@/components/usercenter/Register';
import Login from '@/components/usercenter/Login';
import Culture from '@/components/usercenter/MiguCulture';
import Edit from '@/components/usercenter/Edit';
import Musician from '@/components/home/Musician';
import Singer from '@/components/home/Singer';
import SongInfo from '@/components/home/SongInfo';
import * as serviceWorker from '@/serviceWorker';
import './main.scss';

ReactDOM.render(
  <Provider store = {store}>
  <Router>
    <Switch>
      <Route path='/video' component = { Video } />
      <Route path='/user' component = { User } />
      <Route path='/register' component = { Register } />
      <Route path='/login' component = { Login } />
      <Route path='/culture' component = { Culture } />
      <Route path='/edit' component = { Edit } />
      <Route path='/musician' component = { Musician } />
      <Route path='/singer' component = { Singer } />
      <Route path='/songInfo' component = { SongInfo } />
      <Route path='/' component = { App } />
    </Switch>
  </Router>
  </Provider>
  , document.getElementById('root'));

//如果希望应用程序离线工作并更快地加载，可以更改
//unregister()到下面的register()。注意，这有一些陷阱。
//了解更多关于服务工作者的信息：http://bit.ly/CRA-PWA
// serviceWorker.unregister();
// 如果需要打包并且缓存的时候，改为
// serviceWorker.register();
process.env.NODE_ENV === 'production' ? serviceWorker.register() : serviceWorker.unregister()
